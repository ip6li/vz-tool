#!/usr/bin/env python3

import requests
import pprint
import time
from datetime import datetime
import pandas as pd
import sqlite3

# Reads data from vzlogger

hostname = "localhost"
ID_Z1="***"
ID_Z2="***"

def getData(hostname):
    """
    All Request's come via this function.
    """
    try:
        url = "http://" + hostname
        r = requests.get(url,timeout=60)
        r.raise_for_status()
        return r.json()
    except requests.exceptions.Timeout:
        print("Request: {} failed ".format(url))
    except requests.exceptions.RequestException as e:
        print("Request failed with {}".format(e))

    exit()

def readVzData():
    json = getData(hostname)
    return json

def saveData(jPFRD):
    date_time = datetime.fromtimestamp(time.time())
    str_date_time = date_time.strftime("%Y-%m-%d %H:%M:%S")
    Data = dict()
    Data['Timestamp'] = str_date_time
    Data['c_' + ID_Z1 + '_180'] = jPFRD['data'][0]['tuples'][0][1]
    Data['c_' + ID_Z1 + '_280'] = jPFRD['data'][1]['tuples'][0][1]
    Data['c_' + ID_Z2 + '_180'] = jPFRD['data'][2]['tuples'][0][1]
    Data['c_' + ID_Z2 + '_280'] = jPFRD['data'][3]['tuples'][0][1]

    return Data

def initSQL():
    cn = sqlite3.connect("/tmp/counter_data.sqlite")
    return cn

def InitData(cn):

    # Setup
    # Initialise the DataFrames use pandas to setUp the tables initially
    # This is being lazy, build a proper CREATE

    Data = saveData(readVzData())
    dData = pd.DataFrame(data=Data, index=[0])
    dData.reset_index()
    dData.to_sql("Data",cn,if_exists="append")
    return [dData]

def writeSQL(cn,cur,table,row):
    columns = ', '.join(row.keys())
    placeholders = ':'+', :'.join(row.keys())
    query = 'INSERT INTO %s (%s) VALUES (%s)' % (table,columns, placeholders)
    cur.execute(query, row)
    cn.commit()

def main():
    cn = initSQL()
    cur = cn.cursor()
    dData = InitData(cn)
    while True:
        #try:
            Data = saveData(readVzData())
            print(Data)
            writeSQL(cn,cur,table="Data",row=Data)
            # Loop every 5 seconds
            time.sleep(5)
        #except:
        #    time.sleep(60)
        #    print("sleeping")
    cn.close()
        




if __name__ == "__main__":
    main()

