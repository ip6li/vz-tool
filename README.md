# Volkszähler

Zur Auswertung der Zählerimpulse wird das Repository [Volkszähler](https://volkszaehler.org/) benötigt.
Die Installation des Tools ist unter [vzlogger](https://github.com/volkszaehler/vzlogger) dokumentiert.

## vzlogger.conf

Die Config ist für 2 Zähler des Netzbetreibers vorgesehen, die in einer
Kaskadenschaltung verschaltet sind, damit die PV Netzeinspeisung für
die Wärmepumpe genutzt werden kann.

```
/**
 * vzlogger configuration
 *
 * Use properly encoded JSON with javascript comments
 *
 * Take a look at the wiki for detailed information:
 * http://wiki.volkszaehler.org/software/controller/vzlogger#configuration
 *
 * For an online configuration editor refer to:
 * http://volkszaehler.github.io/vzlogger/
 */

{
    // General settings
    "verbosity": 3,         // log verbosity (0=log_alert, 1=log_error, 3=log_warning, 5=log_info, 10=log_debug, 15=log_finest)
    "log": "/var/log/vzlogger.log", // log file, optional
    "retry": 30,            // http retry delay in seconds

    // Build-in HTTP server
    "local": {
        "enabled": true,   // enable local HTTPd for serving live readings
        "port": 8080,       // TCP port for local HTTPd
        "index": true,      // provide index listing of available channels if no UUID was requested
        "timeout": 30,      // timeout for long polling comet requests in seconds (0 disables comet)
        "buffer": -1        // HTTPd buffer configuration for serving readings, default -1
                            //   >0: number of seconds of readings to serve
                            //   <0: number of tuples to server per channel (e.g. -3 will serve 3 tuples)
    },

    // realtime notification settings
    "push": [
        {
            "url": "http://192.168.2.15:5582"  // notification destination, e.g. frontend push-server
        }
    ],

    // mqtt client support (if ENABLE_MQTT set at cmake generation)
    "mqtt": {
        "enabled": false,  // enable mqtt client. needs host and port as well
        "host": "test.mosquitto.org", // mqtt server addr
        "port": 1883, // 1883 for unencrypted, 8883 enc, 8884 enc cert needed,
        "cafile": "", // optional file with server CA
        "capath": "", // optional path for server CAs. see mosquitto.conf. Specify only cafile or capath
        "certfile": "", // optional file for your client certificate (e.g. client.crt)
        "keyfile": "", // optional path for your client certficate private key (e.g. client.key)
        "keypass": "", // optional password for your private key
        "keepalive": 30, // optional keepalive in seconds.
        "topic": "vzlogger/data", // optional topic dont use $ at start and no / at end
        "id": "", // optional static id, if not set "vzlogger_<pid>" will be used
        "user": "", // optional user name for the mqtt server
        "pass": "", // optional password for the mqtt server
        "retain": false, // optional use retain message flag
        "rawAndAgg": false, // optional publish raw values even if agg mode is used
        "qos": 0, // optional quality of service, default is 0
        "timestamp": false // optional whether to include a timestamp in the payload
    },

    // Meter configuration
    "meters": [
        {
            // SML meter 1 Wärmepumpe
            "enabled": true,               // disabled meters will be ignored (default)
            "allowskip": false,            // errors when opening meter may be ignored if enabled
            "protocol": "sml",             // meter protocol, see 'vzlogger -h' for full list
            "device": "/dev/ttyUSB0",      // meter device
            "aggtime": 10,                 // aggregate meter readings and send middleware update after <aggtime> seconds
            "parity": "8N1",               // Serial parity, 7E1 or 8N1
            "baudrate": 9600,              // Serial baud rate, typically 9600 or 300
            "channels": [{
                "api": "null",      // use MySmartgrid as middleware api
                "type": "sensor",
                "uuid": "***",
                "secretKey": "***",
                "interval": 300,
                //"middleware": "https://localhost",    // identifier for measurement: 1-0:1.8.0
                "identifier": "1-0:1.8.0",  // see 'vzlogger -v20' for an output with all available identifiers/OBIS ids
                "scaler": 1000              // d0 counter is in kWh, so scaling is 1000
            },
            {
                "api": "null",      // use MySmartgrid as middleware api
                "type": "sensor",
                "uuid": "***",
                "secretKey": "***",
                "interval": 300,
                //"middleware": "https://localhost",    // identifier for measurement: 1-0:2.8.0
                "identifier": "1-0:2.8.0",  // see 'vzlogger -v20' for an output with all available identifiers/OBIS ids
                "scaler": 1000              // d0 counter is in kWh, so scaling is 1000
            }]
        },
        {
            // SML meter 2 Haushalt
            "enabled": true,               // disabled meters will be ignored (default)
            "allowskip": false,            // errors when opening meter may be ignored if enabled
            "protocol": "sml",             // meter protocol, see 'vzlogger -h' for full list
            "device": "/dev/ttyUSB1",      // meter device
            "aggtime": 10,                 // aggregate meter readings and send middleware update after <aggtime> seconds
            "parity": "8N1",               // Serial parity, 7E1 or 8N1
            "baudrate": 9600,              // Serial baud rate, typically 9600 or 300
            "channels": [{
                "api": "null",      // use MySmartgrid as middleware api
                "type": "sensor",
                "uuid": "***",
                "secretKey": "***",
                "interval": 300,
                //"middleware": "https://localhost",    // identifier for measurement: 1-0:1.8.0
                "identifier": "1-0:1.8.0",  // see 'vzlogger -v20' for an output with all available identifiers/OBIS ids
                "scaler": 1000              // d0 counter is in kWh, so scaling is 1000
            },
            {
                "api": "null",      // use MySmartgrid as middleware api
                "type": "sensor",
                "uuid": "***",
                "secretKey": "***",
                "interval": 300,
                //"middleware": "https://localhost",    // identifier for measurement: 1-0:2.8.0
                "identifier": "1-0:2.8.0",  // see 'vzlogger -v20' for an output with all available identifiers/OBIS ids
                "scaler": 1000              // d0 counter is in kWh, so scaling is 1000
            }]
        }
    ]
}
```

## Systemd

Den vzlogger startet man mit

```
systemctl enable --now vzlogger.service
```

## Hardware

Als Lesekopf für die Zähler wurde das
[ELV Produkt 155523](https://de.elv.com/elv-bausatz-lesekopf-mit-usb-schnittstelle-fuer-digitale-zaehler-usb-iec-155523)
verwendet. Andere Leseköpfe sollten aber auch funktionieren.

# read-meters.py

Das Python Skript liest die Daten alle 5s von dem vzlogger Daemon ein, der die Momentandaten im JSON Format liefert.
read-meters.py schreibt die Daten zur späteren Verwendung in eine Sqlite Datenbank.

## Wärmepumpe

Den *amtlichen* Stromverbrauch der Außeneinheit der Wärmepumpe lässt sich nun wie folgt ermitteln:

* W[1.8.0] = Z1[1.8.0] - Z2[1.8.0] : Stromverbrauch vom Energieversorger.
* W[2.8.0] = Z2[2.8.0] - Z1[2.8.0] : Stromverbrauch von der PV Anlage.
* W[ges] = W[1.8.0] + W[2.8.0] : Gesamtstromverbrauch.

Der Stromverbrauch der Inneneinheit, Pumpen etc. muss mit einem gesonderten Zähler ermittelt werden.
Es spricht natürlich nichts dagegen, die Daten dieses privaten Zählers auch mit vzlogger zu erfassen.

## Zugang zu den Daten


```
sqlite3 /tmp/counter_data.sqlite "select * from Data order by Timestamp desc limit 10;"
```

## Cleanup

Damit die Datenbank nicht irgendwann das gesamte Filesystem füllt, sollten regelmäßig die
alten Daten gelöscht werden. Dazu dient das Skript *sqlite-cleanup*, das via
Cronjob verwendet werden sollte.

